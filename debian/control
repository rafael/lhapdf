Source: lhapdf
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Lifeng Sun <lifongsun@gmail.com>
Build-Depends: debhelper (>= 8), dh-autoreconf, automake1.11, gfortran, swig, python-dev, dh-python, octave-pkg-dev, gnuplot
Build-Depends-Indep: doxygen-latex, ghostscript
Standards-Version: 3.9.4
Homepage: http://projects.hepforge.org/lhapdf/
Vcs-Git: https://anonscm.debian.org/git/debian-science/packages/lhapdf.git
Vcs-Browser: https://anonscm.debian.org/git/debian-science/packages/lhapdf.git

Package: liblhapdf0v5
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: lhapdf-pdfsets-minimal
Conflicts: liblhapdf0
Replaces: liblhapdf0
Description: [Physics] Les Houches Accord PDF Interface
 LHAPDF provides a unified and easy to use interface to modern PDF sets. It is
 designed to work not only with individual PDF sets but also with the more
 recent multiple "error" sets. It can be viewed as the successor to PDFLIB,
 incorporating many of the older sets found in the latter, including pion and
 photon PDFs. In LHAPDF the computer code and input parameters/grids are
 separated thus allowing more easy updating and no limit to the expansion
 possibilities.

Package: liblhapdf-dev
Section: libdevel
Architecture: any
Depends: liblhapdf0v5 (= ${binary:Version}), ${misc:Depends}
Recommends: lhapdf-pdfsets-minimal
Suggests: lhapdf-ccwrap-doc
Description: [Physics] Les Houches Accord PDF Interface - development files
 LHAPDF provides a unified and easy to use interface to modern PDF sets. It is
 designed to work not only with individual PDF sets but also with the more
 recent multiple "error" sets. It can be viewed as the successor to PDFLIB,
 incorporating many of the older sets found in the latter, including pion and
 photon PDFs. In LHAPDF the computer code and input parameters/grids are
 separated thus allowing more easy updating and no limit to the expansion
 possibilities.
 .
 This package provides development files of LHAPDF, including C++ bindings.

Package: python-lhapdf
Section: python
Architecture: any
Depends: lhapdf-pdfsets-minimal (= ${source:Version}), ${python:Depends}, ${shlibs:Depends}, ${misc:Depends}
Breaks: lhapdf-pdfsets-minimal (<< 5.8.8)
Description: Python Bindings for LHAPDF
 LHAPDF provides a unified and easy to use interface to modern PDF sets. It is
 designed to work not only with individual PDF sets but also with the more
 recent multiple "error" sets. It can be viewed as the successor to PDFLIB,
 incorporating many of the older sets found in the latter, including pion and
 photon PDFs. In LHAPDF the computer code and input parameters/grids are
 separated thus allowing more easy updating and no limit to the expansion
 possibilities.
 .
 This package provides Python bindings for LHAPDF.

Package: octave-lhapdf
Priority: extra
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: octave, liblhapdf0v5 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Recommends: lhapdf-pdfsets-minimal
Description: Octave Bindings for LHAPDF
 LHAPDF provides a unified and easy to use interface to modern PDF sets. It is
 designed to work not only with individual PDF sets but also with the more
 recent multiple "error" sets. It can be viewed as the successor to PDFLIB,
 incorporating many of the older sets found in the latter, including pion and
 photon PDFs. In LHAPDF the computer code and input parameters/grids are
 separated thus allowing more easy updating and no limit to the expansion
 possibilities.
 .
 This package provides Octave bindings for LHAPDF.

Package: lhapdf-pdfsets-minimal
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: python-lhapdf
Description: Minimal PDF Sets of LHAPDF
 LHAPDF provides a unified and easy to use interface to modern PDF sets. It is
 designed to work not only with individual PDF sets but also with the more
 recent multiple "error" sets. It can be viewed as the successor to PDFLIB,
 incorporating many of the older sets found in the latter, including pion and
 photon PDFs. In LHAPDF the computer code and input parameters/grids are
 separated thus allowing more easy updating and no limit to the expansion
 possibilities.
 .
 This package provides a minimal PDF sets required by the test suites of
 LHAPDF and ThePEG.
 .
 Note: the python-lhapdf package provides lhapdf-getdata script to download
 other PDF sets.

Package: lhapdf-ccwrap-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: C++ Bindings for LHAPDF - documentation
 LHAPDF provides a unified and easy to use interface to modern PDF sets. It is
 designed to work not only with individual PDF sets but also with the more
 recent multiple "error" sets. It can be viewed as the successor to PDFLIB,
 incorporating many of the older sets found in the latter, including pion and
 photon PDFs. In LHAPDF the computer code and input parameters/grids are
 separated thus allowing more easy updating and no limit to the expansion
 possibilities.
 .
 This package provides documentation of C++ bindings for LHAPDF.
